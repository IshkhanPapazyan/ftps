import { TestService } from './test.service';
import { Injectable, IServerInitializer, MongoService } from '../dist';

@Injectable()
export class AppInitializer implements IServerInitializer {
    constructor(private testService: TestService, private mongo: MongoService) {

    }

    public init(): void {
        this.testService.init();
        this.mongo.connect({url: 'mongodb://localhost:27017/via-dev'});
    }
}
