import { Injectable, MongoService } from '../dist';
import { Logger } from '../dist';
import * as winston from 'winston';

@Injectable()
export class TransactionService {
    @Logger('TRANSACTION_SERVICE')
    private logger: winston.Logger;

    constructor(
        // private config: ConfigService,
        private mongo: MongoService,
    ) {

    }

    async run<T>(ctx: any, fn: Function): Promise<T> {
        const session = this.mongo.client.startSession();
        const
        return await fn.apply(ctx, null);
    }
}
